package com.lsw.hospitalmanager3.controller;

import com.lsw.hospitalmanager3.entity.HospitalCustomer;
import com.lsw.hospitalmanager3.model.HistoryMedicalItemUpdateRequest;
import com.lsw.hospitalmanager3.model.HistoryRequest;
import com.lsw.hospitalmanager3.service.CustomerService;
import com.lsw.hospitalmanager3.service.HistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/history")
public class HistoryController {

    private final CustomerService customerService;
    private final HistoryService historyService;

    @PostMapping("/new/customer-id/{customerId}")
    public String setHistory(@PathVariable long customerId, @RequestBody @Valid HistoryRequest request) {
        HospitalCustomer hospitalCustomer = customerService.getData(customerId);
        historyService.setHistory(hospitalCustomer,request);

        return "OK";
    }

    @PutMapping("/pay-complete/history-id/{historyId}")
    public String putHistoryByPayComplete(@PathVariable long historyId) {
        historyService.putHistoryByPayComplete(historyId);

        return "OK";
    }

    @PutMapping("/clinc-update/history-id/{historyId}")
    public String putHistoryMedicalItem(@PathVariable long historyId, @RequestBody @Valid HistoryMedicalItemUpdateRequest updateRequest) {
        historyService.putHistoryMedicalItem(historyId,updateRequest);

        return "OK";
    }


}
