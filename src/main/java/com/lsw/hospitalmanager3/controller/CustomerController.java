package com.lsw.hospitalmanager3.controller;

import com.lsw.hospitalmanager3.model.CustomerRequest;
import com.lsw.hospitalmanager3.service.CustomerService;
import lombok.RequiredArgsConstructor;
import net.bytebuddy.asm.Advice;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {
    private  final CustomerService customerService;

    @PostMapping("/new")
    public String setCustomer(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomer(request);

        return "OK";
    }


}
