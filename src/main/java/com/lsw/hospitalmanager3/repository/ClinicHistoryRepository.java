package com.lsw.hospitalmanager3.repository;

import com.lsw.hospitalmanager3.entity.ClinicHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClinicHistoryRepository extends JpaRepository<ClinicHistory, Long> {
}
