package com.lsw.hospitalmanager3.repository;

import com.lsw.hospitalmanager3.entity.HospitalCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HospitlaCustomerRepository extends JpaRepository<HospitalCustomer, Long > {
}
