package com.lsw.hospitalmanager3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HospitalManager3Application {

	public static void main(String[] args) {
		SpringApplication.run(HospitalManager3Application.class, args);
	}

}
