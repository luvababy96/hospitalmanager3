package com.lsw.hospitalmanager3.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
