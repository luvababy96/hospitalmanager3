package com.lsw.hospitalmanager3.service;

import com.lsw.hospitalmanager3.entity.ClinicHistory;
import com.lsw.hospitalmanager3.entity.HospitalCustomer;
import com.lsw.hospitalmanager3.model.HistoryMedicalItemUpdateRequest;
import com.lsw.hospitalmanager3.model.HistoryRequest;
import com.lsw.hospitalmanager3.repository.ClinicHistoryRepository;
import com.lsw.hospitalmanager3.repository.HospitlaCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class HistoryService {

    private final ClinicHistoryRepository clinicHistoryRepository;

    public void setHistory(HospitalCustomer hospitalCustomer, HistoryRequest historyRequest) {
        ClinicHistory addData = new ClinicHistory.ClinicHistoryBuilder(hospitalCustomer, historyRequest).build();

        clinicHistoryRepository.save(addData);
    }

    public void putHistoryByPayComplete(long id) {
        ClinicHistory originData = clinicHistoryRepository.findById(id).orElseThrow();
        originData.putCompletePay();

        clinicHistoryRepository.save(originData);

    }

    public void putHistoryMedicalItem(long id, HistoryMedicalItemUpdateRequest updateRequest) {
        ClinicHistory originData = clinicHistoryRepository.findById(id).orElseThrow();
        originData.putMedicalItem(updateRequest);

        clinicHistoryRepository.save(originData);
    }

}
