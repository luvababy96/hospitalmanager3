package com.lsw.hospitalmanager3.service;

import com.lsw.hospitalmanager3.entity.HospitalCustomer;
import com.lsw.hospitalmanager3.model.CustomerRequest;
import com.lsw.hospitalmanager3.repository.HospitlaCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final HospitlaCustomerRepository hospitlaCustomerRepository;

    public void setCustomer(CustomerRequest request) {
        HospitalCustomer addData = new HospitalCustomer.HospitalCustomerBuilder(request).build();

        hospitlaCustomerRepository.save(addData);
    }

    public HospitalCustomer getData(long id) {
        return hospitlaCustomerRepository.findById(id).orElseThrow();
    }





}
